'use strict';

var exec = require('child_process').exec;

var Promises = require('bluebird');
var utils = require('./lib/utils');
var config = require('./lib/config');

var boilerplateName = 'fusionary-fed';

var tasks = {
  installBoilerplate: function installBoilerplate() {
    utils.log('Installing ' + boilerplateName + '…', 'yellow');

    return new Promises(function(resolve, reject) {
      exec('npm install ' + boilerplateName + '@latest', function(err) {

        if (err) {
          utils.log(err, 'red');
          reject(err);
        } else {

          utils.log('Finished installing ' + boilerplateName, 'green');

          resolve();
        }
      });
    });
  },
  getFedDeps: function getFedDeps() {

    let fedPkg = require('./node_modules/' + boilerplateName + '/templates/package.json');

    return Object.keys(fedPkg.devDependencies);
  },
  getConfigDeps: function getConfigDeps(fedDeps) {
    let push = Array.prototype.push;
    let configDeps = [];
    let devDeps = config.devDependencies;
    devDeps.forEach((item) => {
      if (item.choices) {
        push.apply(configDeps, item.choices);
      }
    });

    return {
      fed: fedDeps.sort(),
      config: configDeps.sort()
    };
  },
  comparePackages: function comparePackages(deps) {
    let only = {
    };

    Object.keys(deps).forEach((item) => {
      only[item] = [];
    });

    deps.fed.forEach((item) => {
      if (deps.config.indexOf(item) === -1) {
        only.fed.push(item);
      }
    });

    deps.config.forEach((item) => {
      if (deps.fed.indexOf(item) === -1) {
        only.config.push(item);
      }
    });

    utils.log('Only in…', 'yellow');
    console.log(JSON.stringify(only, null, 2));
  }
};

tasks.installBoilerplate()
.then(tasks.getFedDeps)
.then(tasks.getConfigDeps)
.then(tasks.comparePackages);
