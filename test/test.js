// jscs disable: maximumLineLength

var path = require('path');

var Promises = require('bluebird');
var fs = require('fs-extra');
var mocha = require('mocha');
var sinon = require('sinon');
var sinonchai = require('sinon-chai');
var foldersize = require('get-folder-size');
var expect = require('chai').use(require('sinon-chai')).expect;
var spawnSync = require('child_process').spawnSync;

process.chdir(path.join(process.cwd(), 'test', 'testarea'));

var cwd = process.cwd();
var setup = require('../setup');
var config = require('../lib/config');

describe('Fusionary-Utils', function() {
  describe('Setup', function() {
    it('createPackage creates a .json package', function(done) {
      var json = {
        createPkg: true,
        repository: 'srsly.noneofyourbuisness.git',
        devDependencies: {
          'get-folder-size': '0.1.0'
        }
      };
      setup.responses.createPackage(json);
      fs.readFile('package.json', function(err, data) {

        if (err) {
          throw err;
        }
        var objData = JSON.parse(data);
        expect(objData.repository.type).to.equal('git');
        expect(objData.repository.url).to.equal('srsly.noneofyourbuisness.git');
        expect(objData.devDependencies['get-folder-size']).to.equal('0.1.0');
        expect(objData.author.name).to.equal('Fusionary');
        expect(objData.author.email).to.equal('info@fusionary.com');
        expect(objData.author.url).to.equal('http://fusionary.com/');
        expect(objData.version).to.equal('1.0.0');
        expect(objData.private).to.equal(true);
        done();
      });
    });
    it('gitInit makes a git repo', function(done) {
      var answers = {
        gitInit: true
      };
      setup.responses.gitInit(answers).then(function() {
        foldersize(path.join(cwd, '.git'), function(err, fsize) {
          var gitExists = fsize > 1700;
          expect(gitExists).to.equal(true);
          done();
        });
      });
    });
    it('copyFiles (Should prompt for a single file to be copied)', function(done) {
      var info = {
        extra: 1,
        certified: true,
        bpCount: 1,
        projCount: 0
      };
      setup.responses.copyFiles(info);
      done();
    });
    it('updateDependencies adds the right dependencies', function(done) {
      var answers = {
        'modules': true,
        'depConfirm-add-postcss': true,
        'depConfirm-add-suitcss': true,
        'depConfirm-add-webpack': true,
        'depConfirm-add-shipit': true,
        'dep-add-optional': [
          'get-folder-size',
          'normalize.css'
        ]
      };
      setup.responses.updateDependencies(answers).then(function(mods) {
        expect(mods.indexOf('bluebird')).to.not.equal(-1);
        expect(mods.indexOf('postcss-import')).to.not.equal(-1);
        expect(mods.indexOf('suitcss-base')).to.not.equal(-1);
        expect(mods.indexOf('babel-core')).to.not.equal(-1);
        expect(mods.indexOf('shipit-cli')).to.not.equal(-1);
        expect(mods.indexOf('get-folder-size')).to.not.equal(-1);
        expect(mods.indexOf('normalize.css')).to.not.equal(-1);
        done();
      });
    });
    it('cleanup cleans up what it\'s told', function(done) {

      var webpackFiles = config.clean.webpack.files;
      var answers = {
        clean: ['webpack']
      };

      // 1. Create the files
      return Promises.each(webpackFiles, function(file) {
        return fs.outputFileAsync(file, 'dummy text');
      })
      .then(() => {
        // 2. Run the cleanup operation
        return setup.responses.cleanup(answers)
        .then(function() {
          var dne = 'file does not exist';
          // 3. Make sure each file has been deleted
          return Promises.each(webpackFiles, function(file) {
            return fs.readFileAsync(file)
            .then(() => {
              expect(dne).to.equal(file);
            })
            .catch(() => {
              expect(dne).to.equal(dne);
            });
          })
          .then(() => {
            done();
          })
          .catch(done);
        });
      });
      // Obligatory callback 'pyramid of doom'

    });

    it('updateDotEnv creates a correct .env file', function(done) {
      var answers = {
        'S3_BUCKET': 'SUM_BUKKIT',
        'SECRET_KEY': 'PCODE',
        'PRODUCTION': 'FALSE'
      };
      setup.responses.updateDotEnv(answers).then(function() {
        fs.readFile('.env', function(err, data) {
          if (data === 'S3_BUCKET=SUM_BUKKIT\nSECRET_KEY=PCODE\nPRODUCTION=FALSE\n') {
            done();
          }
        });
      });
      done();
    });
  });
});
