'use strict';

var path = require('path');
var execSync = require('child_process').execSync;
var chalk = require('chalk');

var config = require('./config');
var utils = require('./utils');

module.exports = {

  package: function(create) {

    var prompts = [
      {
        name: 'name',
        message: 'Project name',
        type: 'input',
        filter: function(txt) {
          return txt.replace(/[\s\-]+/, '-');
        },
        default: function() {
          return path.basename(process.cwd());
        },
        when: function(answers) {
          return answers.createPkg !== false;
        }
      },
      {
        name: 'version',
        message: 'Version',
        type: 'input',
        default: config.packageDefaults.version,
        when: function(answers) {
          return answers.createPkg !== false;
        }
      },
      {
        name: 'description',
        message: 'Description',
        type: 'input',
        when: function(answers) {
          return answers.createPkg !== false;
        }
      },
      {
        name: 'repository',
        message: 'git repository',
        type: 'input',
        default: function() {
          var remoteUrl = '';

          try {
            execSync('git config --get remote.origin.url');
          } catch (err) {}

          return remoteUrl;
        },
        when: function(answers) {
          return answers.createPkg !== false;
        }
      },
      {
        name: 'license',
        message: 'License',
        type: 'input',
        default: config.packageDefaults.license,
        when: function(answers) {
          return answers.createPkg !== false;
        }
      }
    ];

    if (create) {
      prompts.unshift({
        name: 'createPkg',
        message: 'You must have a package.json file to proceed. Would you like to create one now?',
        type: 'confirm',
        default: true
      });
    }

    return prompts;
  },
  gitInit: function gitInit() {
    var prompts = [
      {
        message: 'Initialize this project as a git repository?',
        name: 'gitInit',
        type: 'confirm'
      }
    ];

    return prompts;
  },
  chooseRepo: function chooseRepo(repos) {
    var choices = [
      {
        name: repos.pkg,
        value: 'setRepoGitConfig'
      },
      {
        name: repos.config,
        value: 'setRepoPkg'
      },
      {
        name: 'Both! Keep them different',
        value: 'keep'
      }
    ];

    var prompts = [
      {
        message: 'Your git repo in package.json is different from the one in .git/config. Choose one to keep.',
        name: 'chooseRepo',
        type: 'list',
        choices: choices,
      }
    ];

    return prompts;
  },
  dependencies: function dependencies(depGroups, installed, args) {
    var dependencyPrompts = [];
    var flattenedChoices = [];
    var boilerplateDeps = [];
    var lastGroup = {};
    var when = function(answers) {
      return answers.modules;
    };

    var msg = [
      'Would you like to install node modules',
      '(and add them to devDependencies in package.json)?'
    ].join(' ');

    var modulesPrompt = {
      message: msg,
      name: 'modules',
      type: 'confirm'
    };

    depGroups.forEach(function(group) {
      var groupChoices = [];

      group.choices.forEach(function(choice) {
        if (!installed[choice]) {
          flattenedChoices.push(choice);
          groupChoices.push({
            name: choice,
            value: choice,
            checked: true
          });
        }
      });

      // For confirming addition of an entire GROUP of dependencies,
      // only ask if none of those dependencies has already been installed
      if (group.type === 'confirm' && group.choices.length === groupChoices.length) {
        dependencyPrompts.push({
          name: 'depConfirm-' + 'add-' + group.name,
          message: 'Do you want to install ' + chalk.yellow(group.name) + ' packages?',
          type: group.type,
          when: when
        });
      }

      // For choosing among extra modules
      if (group.type === 'checkbox' && groupChoices.length) {
        dependencyPrompts.push({
          name: 'dep-' + 'add-' + group.name,
          message: 'Which ' + group.name + ' packages would you like to add?',
          type: group.type,
          choices: groupChoices,
          when: when
        });
      }
    });

    boilerplateDeps = utils.mergeBoilerplateDependencies(
      args.boilerplateDir,
      flattenedChoices || [],
      installed
    );

    // If there are more dependencies in the boilerplate package.json,
    // add those to the "optional" group if it exists (otherwise create new group for them)
    if (boilerplateDeps.length) {
      lastGroup = dependencyPrompts[dependencyPrompts.length - 1] || {};

      if (lastGroup.name === 'dep-add-optional') {
        [].push.apply(dependencyPrompts[dependencyPrompts.length - 1].choices, boilerplateDeps);
      } else {
        dependencyPrompts.push({
          name: 'dep-add-extras',
          message: 'Which extra packages from ' + args.boilerplateName + ' would you like to add?',
          type: 'checkbox',
          choices: boilerplateDeps,
          when: when
        });
      }
    }

    // Add the overall question to the BEGINNING of the array.
    if (dependencyPrompts.length) {
      dependencyPrompts.unshift(modulesPrompt);
    }

    return dependencyPrompts;
  },

  cleanFiles: function cleanFiles(msg) {
    var prompts = [
      {
        name: 'clean',
        message: msg,
        type: 'confirm',
      }
    ];

    return prompts;
  },

  askDotEnv: function askDotEnv() {
    var prompts = [
      {
        message: 'Would you like to update your .env file?',
        name: 'dotenv',
        type: 'confirm'
      }
    ] ;

    return prompts;
  },
  dotEnv: function(json, pkg) {
    var prompts = [];

    for (var el in json) {

      prompts.push({
        name: el,
        message: 'Enter a value (or null) for the .env property ' + chalk.yellow(el),
        type: 'input',
        default: json[el] === 'example.com' ? pkg.name + '.dev' : json[el]
      });
    }

    return prompts;
  }
};
