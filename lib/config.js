'use strict';

module.exports = {
  // for setup.js:
  clean: {
    suitcss: {
      lines: {
        'app/assets/css/app.css': 'suitcss',
      }
    },
    postcss: {
      lines: {
        'gulpfile.js/config/index.js': 'postcss',
        'gulpfile.js/tasks/build/css.js': 'postcss',
        'gulpfile.js/tasks/build/index.js': 'css',
        'gulpfile.js/tasks/lint/css.js': 'postcss',
      },
      files: [
        'gulpfile.js/config/postcss.js',
        'gulpfile.js/tasks/build/css.js',
      ]
    },
    webpack: {
      lines: {
        'gulpfile.js/config/index.js': 'webpack',
        'gulpfile.js/tasks/build/css.js': 'webpack',
        'gulpfile.js/tasks/build/js.js': 'webpack',
      },
      files: [
        'gulpfile.js/config/webpack.js',
        'gulpfile.js/tasks/serve/index.js',
      ]

    },
    shipit: {
      lines: {
        'gulpfile.js/config/index.js': 'shipit',
        'gulpfile.js/index.js': 'shipit',
      },
      files: [
        'gulpfile.js/config/shipit.js',
        'gulpfile.js/tasks/shipit/index.js',
      ]
    }
  },
  packageDefaults: {
    author: {
      name: 'Fusionary',
      email: 'info@fusionary.com',
      url: 'http://fusionary.com/',
    },
    version: '1.0.0',
    private: true,
    license: 'MIT',
  },
  devDependencies: [
    {
      name: 'required',
      type: 'required',
      choices: [
        'bluebird',
        'inquirer',
        'browser-sync',
        'chalk',
        'del',
        'dotenv',
        'eslint-config-kswedberg',
        'favicons',
        'fusionary-fed',
        'git-rev',
        'gulp',
        'gulp-debug',
        'gulp-eslint',
        'gulp-if',
        'gulp-imagemin',
        'gulp-json-editor',
        'gulp-modernizr',
        'gulp-rev',
        'gulp-sourcemaps',
        'gulp-svg-sprite',
        'imagemin-mozjpeg',
        'imagemin-pngquant',
        'lodash',
        'merge-stream',
        'require-dir',
        'yargs',
        'gulp-data',
        'gulp-nunjucks-render',
        'nunjucks',
        'mkdirp',
        'chai',
        'mocha',
        'nightwatch',
        'request',
        'slugify',
      ],
    },
    {
      name: 'postcss',
      type: 'confirm',
      choices: [
        'gulp-postcss',
        'autoprefixer',
        'cssnano',
        'pixrem',
        'pleeease-filters',
        'postcss-assets',
        'postcss-bem-linter',
        'postcss-calc',
        'postcss-color-function',
        'postcss-color-gray',
        'postcss-color-hex-alpha',
        'postcss-color-hwb',
        'postcss-color-rebeccapurple',
        'postcss-color-rgba-fallback',
        'postcss-custom-media',
        'postcss-custom-properties',
        'postcss-custom-selectors',
        'postcss-easings',
        'postcss-extend',
        'postcss-font-variant',
        'postcss-import',
        'postcss-map',
        'postcss-media-minmax',
        'postcss-mixins',
        'postcss-nested',
        'postcss-pseudo-class-any-link',
        'postcss-pseudo-class-enter',
        'postcss-pseudoelements',
        'postcss-reporter',
        'postcss-selector-matches',
        'postcss-selector-not',
        'postcss-size',
        'postcss-strip-units',
      ],
    },
    {
      name: 'suitcss',
      type: 'confirm',
      choices: [
        'suitcss-base',
        'suitcss-components-arrange',
        'suitcss-components-button',
        'suitcss-components-flex-embed',
        'suitcss-components-grid',
        'suitcss-utils-align',
        'suitcss-utils-display',
        'suitcss-utils-layout',
        'suitcss-utils-offset',
        'suitcss-utils-position',
        'suitcss-utils-size',
        'suitcss-utils-text',
      ]
    },
    {
      name: 'webpack',
      type: 'confirm',
      choices: [
        'babel-core',
        'babel-loader',
        'babel-plugin-transform-runtime',
        'babel-polyfill',
        'babel-plugin-lodash',
        'babel-preset-es2015',
        'babel-preset-stage-3',
        'expose-loader',
        'exports-loader',
        'nunjucks-loader',
        'webpack',
        'webpack-dev-middleware',
        'webpack-stream',
      ]
    },
    {
      name: 'shipit',
      type: 'confirm',
      choices: [
        'shipit-assets',
        'shipit-captain',
        'shipit-cli',
        'shipit-db',
        'shipit-deploy',
        'shipit-fusionary',
        'shipit-npm',
        'shipit-shared',
        'shipit-ssh',
      ]
    },
    {
      name: 'optional',
      type: 'checkbox',

      choices: [
        'jquery',
        'fmjs',
      ]
    }
  ]
};
