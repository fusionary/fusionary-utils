// jshint unused:true
'use strict';

var Promises = require('bluebird');
var fs = Promises.promisifyAll(require('fs-extra'));
var exec = require('child_process').exec;
var path = require('path');

var glob = require('glob');
var globAsync = Promises.promisify(glob);

var utils = require('./utils');
var cwd = process.cwd();

module.exports = {

  getOverwritable: function getOverwritable(options) {
    return globAsync(options.boilerplateDir + '/**')
    .map(function(file) {
      var rel = path.relative(options.boilerplateDir, file);

      return {
        // Set up properties for inquirer checkbox prompt ahead of time here:
        name: rel,
        value: rel,
        // Make all checked by default, unless it's a markdown file
        checked: !/\.md$/.test(rel),
        boilerplate: file,
        project: path.join(process.cwd(), rel)
      };
    })
    .filter(function(file) {
      //
      if (/package\.json$/.test(file.name)) {
        return false;
      }

      return fs.statAsync(file.project)
      .then(function(stat) {
        return stat.isFile();
      })
      .catch(function() {
        return false;
      });
    })
    .then(function(files) {
      return {
        files: files
      };
    });
  },

  getCopyable: function getCopyable(opts) {
    var info = {
      bpCount: 0,
      projCount: 0,
      files: [],
    };
    var boilerplateFiles = glob.sync(opts.boilerplateDir + '/**');

    // Clean up list of files. Don't include the gitignore file:
    boilerplateFiles = boilerplateFiles.filter(function(file) {
      return file && !/gitignore/.test(file);
    });

    info.bpCount = boilerplateFiles.length;

    return Promises
    .each(boilerplateFiles, function(bpFile) {
      var file = path.relative(opts.boilerplateDir, bpFile);

      if (!file) {
        return Promises.resolve(true);
      }

      return fs.statAsync(file)
      .then(function() {
        info.projCount++;
      })
      .catch(function() {
        info.files.push({
          boilerplate: bpFile,
          project: file,
          name: file,
          value: file
        });

        return Promises.resolve(true);
      });
    })
    .then(function() {
      info.extra = info.files.length;

      return Promises.resolve(info);
    })
    .catch(function() {
      info.certified = true;

      return Promises.resolve(info);
    });
  },

  copyFiles: function overwriteFiles(all, files, clobber) {
    var msg = clobber ? 'Overwrote ' : 'Copied ';

    return Promises
    .filter(all, function(file) {
      return files.indexOf(file.value) !== -1;
    })
    .each(function(file) {
      return fs.copyAsync(file.boilerplate, file.project, {clobber: clobber})
      .then(function() {
        utils.log(msg + file.value, 'green');
      });
    });
  },

  copyFilesInitial: function copyFilesInitial(opts) {
    var boilerplateFiles = glob.sync(opts.boilerplateDir + '/**');
    var runderGitignore = /_gitignore/;

    return Promises
    .each(boilerplateFiles, function(bpFile) {
      var file = path.relative(opts.boilerplateDir, bpFile);

      if (runderGitignore.test(file)) {
        file = file.replace(runderGitignore, '.gitignore');
      }

      return fs.copyAsync(bpFile, file, {clobber: false})
      .then(function() {
        utils.log('Copied ' + file, 'green');
      });
    });
  },

  modules: function modules(opts) {
    var args = [];
    var mods = opts.modules || [];
    var boilerDeps = require(path.join(opts.boilerplateDir, 'package.json')).devDependencies || {};
    var pkgDeps = 0;
    var pkgContent;
    var logInstall = function logInstall() {
      console.log('✋  About to run `npm install`.');
      console.log('☕  This could take a while, so grab some coffee or something…');
    };

    if (!mods.length) {
      console.log('No modules to install');

      return Promises.resolve();
    }

    mods.forEach(function(mod) {

      if (boilerDeps[mod] && !opts.pkg.devDependencies[mod]) {
        opts.pkg.devDependencies[mod] = boilerDeps[mod];
        pkgDeps++;
      } else {
        args.push(mod);
      }
    });
    pkgContent = JSON.stringify(opts.pkg, null, 2);

    return fs.writeFileAsync(path.join(cwd, 'package.json'), pkgContent)
    .then(function() {
      if (!pkgDeps) {
        utils.log('No devDependencies to install.', 'yellow');

        return;
      }

      return new Promises(function(resolve) {
        logInstall();

        var npm = exec('npm install', function() {
          utils.log('\nFinished installing node modules from fusionary-fed', 'green');
          resolve();
        });
        npm.stdout.on('data', function(data) {
          console.log(data.toString());
        });

        npm.stderr.on('data', function(err) {
          console.log(err.toString());
        });
      });
    })
    .then(function() {
      if (!args.length) {
        utils.log('No extra modules to install.', 'yellow');

        return;
      }

      args = ['npm', 'install', '--save-dev'].concat(args);

      return new Promises(function(resolve) {
        if (!pkgDeps) {
          logInstall();
        } else {
          console.log('✋✋  Sorry. One more round of `npm install`.');
          console.log('☕☕  Maybe another cup of coffee?');
        }

        var npm = exec(args.join(' '), function() {
          utils.log('\nFinished installing extra node modules', 'green');
          resolve();
        });
        npm.stdout.on('data', function(data) {
          console.log(data.toString());
        });

        npm.stderr.on('data', function(err) {
          console.log(err.toString());
        });
      });
    })
    .catch(function(err) {
      console.log('Uh oh. Some error occurred');
      console.log(err);
    });

  }
};
