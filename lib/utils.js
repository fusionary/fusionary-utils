'use strict';
// jscs: disable maximumLineLength

var path = require('path');
var exec = require('child_process').exec;
var Promises = require('bluebird');
var fs = Promises.promisifyAll(require('fs-extra'));
var ini = require('ini');
var glob = require('glob');
var chalk = require('chalk');
var LineByLineReader = require('line-by-line');

var config = require('./config');

var utils = module.exports = {

  // COMMON UTILS
  readJSON: function(file) {
    try {
      return require(path.resolve('./', file));
    } catch (err) {
      return {};
    }
  },
  writeJSON: function(file, json) {
    if (typeof json === 'object') {
      json = JSON.stringify(json, null, 2);
    }
    json += '\n';
    fs.writeFileSync(file, json);
  },
  extend: function() {
    var arg, prop;
    var args = [].slice.call(arguments);
    var al = args.length;
    var firstArg = al === 1 ? {} : args.shift();

    while (--al > -1) {
      arg = args[al];

      if ((typeof arg === 'object' && arg !== null) || typeof arg === 'function') {
        for (prop in arg) {
          if (arg.hasOwnProperty(prop)) {
            firstArg[prop] = arg[prop];
          }
        }
      }
    }

    return firstArg;
  },

  log: function log(msg, color) {
    if (color) {
      msg = chalk[color](msg);
    }
    console.log(msg);
  },
  logError: function logError(msg) {
    utils.log(msg, 'red');
  },

  // SETUP UTILS
  boilerplateWarning: function(name) {

    var msg = '\n**** You are in a boilerplate repo (' + name + '). Do NOT run the setup script here.';

    if (/boilerplate|fed|fusionary-utils/.test(name) && process.argv.indexOf('--force') === -1) {
      utils.log(msg, 'red');
      utils.log('If you know what you\'re doing, run:\n  setup --force');

      return true;
    }

    return false;
  },

  getRepo: function getRepo(pkg) {
    var configPath = path.join(process.cwd(), '.git', 'config');
    var repo = {
      pkg: pkg && pkg.repository && pkg.repository.url || ''
    };

    return fs.readFileAsync(configPath, 'utf8')
    .then(function(data) {
      var json = ini.parse(data);
      var remote = json['remote "origin"'];
      repo.config = remote && remote.url || '';

      return Promises.resolve(repo);
    })
    .catch(function() {
      var msg = [
        '',
        'You are not currently in a git repository.',
        'You should run `git init` at some point to initialize one.',
        '',
      ].join('\n');

      utils.log(msg, 'red');
    });
  },
  setRepoPkg: function setRepoPkg(pkg, repos) {
    pkg.repository = {
      url: repos.config,
      type: 'git'
    };

    return fs.writeFileAsync(path.join(process.cwd(), 'package.json'), JSON.stringify(pkg, null, 2));
  },
  setRepoGitConfig: function setRepo(pkg, repos) {
    var configPath = path.join(process.cwd(), '.git', 'config');
    var repo = repos.pkg;

    if (!repo) {
      return Promises.resolve();
    }

    return fs.readFileAsync(configPath, 'utf8')
    .then(function(data) {
      var json = ini.parse(data);
      var remote = json['remote "origin"'] || {};
      var master = json['branch "master"'] || {};

      remote.url = repo;
      remote.fetch = remote.fetch || '+refs/heads:refs/remotes/origin/*';

      master.remote = master.remote || 'origin';
      master.merge = master.merge || 'refs/heads/master';

      json['remote "origin"'] = remote;
      json['branch "master"'] = master;

      return Promises.resolve(json);
    })
    .then(function(json) {
      var data = ini.stringify(json);

      // return fs.writeFileAsync(configPath, data);
      return fs.writeFileAsync(path.join(process.cwd(), '.git', 'config'), data);
    })
    .catch(function() {
      utils.log('This is not a git repo. Run `git init`', 'red');
    });
  },

  npmInstall: function npmInstall(json, options, cb) {
    utils.log('Installing ' + options.boilerplateName + '…', 'yellow');
    json.repo = json.repository && json.repository.url || '';

    var installing = exec('npm install -D ' + options.boilerplateName, function(err) {
      if (err) {
        utils.log(err, 'red');
      } else {
        utils.log('Finished installing ' + options.boilerplateName, 'green');
        cb(json);
      }
    });

    installing.stdout.pipe(process.stdout);
  },
  mergeBoilerplateDependencies: function mergeBoilerplateDependencies(boilerplateDir, flattenedChoices, installed) {
    var tmplPackageDeps = require(path.join(boilerplateDir, 'package.json')).devDependencies;
    installed = Object.keys(installed) || [];
    flattenedChoices = flattenedChoices.concat(installed);

    var extras = [];
    Object.keys(tmplPackageDeps).forEach(function(pkgDep) {
      if (flattenedChoices.indexOf(pkgDep) === -1) {
        extras.push({
          name: pkgDep,
          value: pkgDep,
        });
      }
    });

    return extras;
  },

  getRequiredDependencies: function getRequiredDependencies(depGroups, installed) {
    installed = Object.keys(installed) || [];
    var deps = [];

    depGroups.forEach(function(group) {
      if (group.type !== 'required') {
        return;
      }

      group.choices.forEach(function(dep) {
        if (installed.indexOf(dep) === -1) {
          deps.push(dep);
        }
      });

    });

    return deps;
  },

  getConfirmedDependencies: function getConfirmedDependencies(depGroups, groupName) {
    var deps = [];
    depGroups.forEach(function(group) {
      if (group.name === groupName) {
        deps = group.choices;
      }
    });

    return deps;
  },
  buildDependencyPrompts: function buildDependencyPrompts(depGroups, installed, args) {
    var dependencyPrompts = [];
    var flattenedChoices = [];
    var boilerplateDeps = [];
    var lastGroup = {};
    depGroups.forEach(function(group) {
      var groupChoices = [];

      group.choices.forEach(function(choice) {
        if (!installed[choice]) {
          flattenedChoices.push(choice);
          groupChoices.push({
            name: choice,
            value: choice,
            checked: true
          });
        }
      });

      // For confirming addition of an entire GROUP of dependencies,
      // only ask if none of those dependencies has already been installed
      if (group.type === 'confirm' && group.choices.length === groupChoices.length) {
        dependencyPrompts.push({
          name: 'depConfirm-' + 'add-' + group.name,
          message: 'Do you want to install ' + chalk.yellow(group.name) + ' packages?',
          type: group.type,
        });
      }

      //
      if (group.type === 'checkbox' && groupChoices.length) {
        dependencyPrompts.push({
          name: 'dep-' + 'add-' + group.name,
          message: 'Which ' + group.name + ' packages would you like to add?',
          type: group.type,
          choices: groupChoices
        });
      }
    });

    boilerplateDeps = utils.mergeBoilerplateDependencies(args.boilerplateDir, flattenedChoices || [], installed);

    // If there are more dependencies in the boilerplate package.json,
    // add those to the "optional" group if it exists (otherwise create new group for them)
    if (boilerplateDeps.length) {
      lastGroup = dependencyPrompts[dependencyPrompts.length - 1] || {};

      if (lastGroup.name === 'dep-add-optional') {
        [].push.apply(dependencyPrompts[dependencyPrompts.length - 1].choices, boilerplateDeps);
      } else {
        dependencyPrompts.push({
          name: 'dep-add-extras',
          message: 'Which extra packages from ' + args.boilerplateName + ' would you like to add?',
          type: 'checkbox',
          choices: boilerplateDeps,
        });
      }
    }

    return dependencyPrompts;
  },

  readDotEnvFiles: function readDotEnvFiles(file) {
    var env = require('dotenv');

    return fs.readFileAsync(file, 'utf8')
    .then(function(data) {
      var json = env.parse(data);

      return Promises.resolve(json);
    })
    .catch(function() {});
  },
  jsonToEnv: function jsonToEnv(json) {
    var env = [];

    for (var el in json) {
      if (json[el] !== 'null') {
        env.push(el + '=' + json[el]);
      }
    }

    return env.join('\n') + '\n';
  },

  cleanFiles: function cleanFiles(groups) {
    var files = [];

    if (!Array.isArray(groups)) {
      groups = [groups];
    }

    groups.forEach(function(group) {
      var filesToClean = config.clean[group].files;

      [].push.apply(files, filesToClean);
      // [].push.apply(files, glob.sync('gulpfile.js/*' + file + '*'));
    });

    var callback = function callback() {
      utils.log('\nDeleted the following files:', 'yellow');
      console.log(files);
    };

    return Promises.each(files, function(file) {
      return fs.removeAsync(file);
    })
    .then(callback)
    .catch(utils.logError);
  },

  cleanFileLines: function cleanFileLines(fileObj, resolve) {
    var file = fileObj.file;
    var txt = fileObj.text;
    var lr = new LineByLineReader(file);
    var keep = [];
    var remove = [];

    lr.on('line', function(line) {
      if (line.indexOf(txt) === -1) {
        keep.push(line);
      } else {
        remove.push(line);
      }
    });

    lr.on('end', function() {
      var content = keep.join('\n') + '\n';

      return fs.writeFileAsync(file, content)
      .then(function() {
        var msg = 'No lines to remove';

        if (remove.length) {
          msg = [
            'Removed ',
            remove.length,
            utils.singularPlural(remove.length, ['file', 'files']),
            'with',
            '"' + txt + '"'
          ].join(' ');
        }

        console.log(chalk.yellow(msg), 'from', file);
        resolve();
      })
      .catch(function(err) {
        resolve();

        return console.log(err);
      });
    });
  },
  cleanLines: function cleanLines(groups) {
    var files = [];
    groups.forEach(function(group) {
      Object.keys(config.clean[group].lines).forEach(function(file) {
        var globbed = glob.sync(file);
        var txt = config.clean[group].lines[file];

        globbed.forEach(function(f) {
          files.push({file: f, text: txt});
        });
      });
    });

    return Promises.each(files, function(file) {
      return new Promises(function(resolve) {
        utils.cleanFileLines(file, resolve);
      });
    })
    .then(function() {
      console.log(chalk.yellow('Done cleaning lines from files.'));

      return groups;
    });
  },
  cleanPackage: function cleanPackage(groups) {
    var pkgPath = path.join(process.cwd(), 'package.json');
    var pkg = require(pkgPath);
    var content = '';

    config.devDependencies.forEach(function(dependGroup) {
      if (groups.indexOf(dependGroup.name) !== -1) {
        dependGroup.choices.forEach(function(dependency) {
          delete pkg.devDependencies[dependency];
        });
      }
    });

    content = JSON.stringify(pkg, null, 2);

    return fs.writeFileAsync(pkgPath, content)
    .then(function() {
      console.log(chalk.yellow('\nRemoved devDependencies'), 'from package.json');

      return groups;
    });
  },

  singularPlural: function(num, arr) {
    return num === 1 ? arr[0] : arr[1];
  }
};
