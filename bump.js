'use strict';

var chalk = require('chalk');

console.log(chalk.red('\n~~~~~~ Sorry, but the bump command has been removed. ~~~~~~'));
console.log(chalk.green('\nFeel free to use *ump* instead! To install:'));
console.log(chalk.cyan('\n\t npm install -g ump'));
