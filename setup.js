// jshint unused:true

'use strict';
var path = require('path');

var exec = require('child_process').exec;
var Promises = require('bluebird');
var fs = Promises.promisifyAll(require('fs-extra'));

var inquirer = require('inquirer');
var cwd = process.cwd();
var chalk = require('chalk');

var utils = require('./lib/utils');
var config = require('./lib/config');
var setupTasks = require('./lib/setup-tasks');
var questions = require('./lib/questions');

var boilerplateName = 'fusionary-fed';
var boilerplateDir = path.join(cwd, 'node_modules', boilerplateName, 'templates');

var options = {
  jQuery: '1.11.3',
  boilerplateName: boilerplateName,
  boilerplateDir: boilerplateDir,
  configDir: path.resolve('./', 'config'),
};

var pkg = {};

var responses = {
  createPackage: function(json) {
    var repo = {
      type: 'git',
      url: ''
    };

    if (!json.createPkg) {
      utils.log('Sorry. Can\'t continue until package.json exists', 'red');

      return;
    }

    json.author = {
      name: 'Fusionary',
      email: 'info@fusionary.com',
      url: 'http://fusionary.com/',
    };

    json.version = '1.0.0';
    json.private = true;

    if (json.repository) {
      repo.url = json.repository;
    }

    json.repository = repo;

    delete json.createPkg;
    pkg = json;

    return fs.writeFileAsync(path.join(cwd, 'package.json'), JSON.stringify(json, null, 2))
    .catch(utils.logError);

  },
  gitInit: function(answers) {
    if (answers.gitInit) {

      return new Promises(function(resolve, reject) {
        exec('git init', function(err, stdout, stderr) {
          if (err) {
            reject(err);
          } else {
            resolve({
              stdout: stdout,
              stderr: stderr
            });
          }
        });
      })
      .then(function(result) {
        if (result.stderr) {
          console.log(result.stderr.toString());
        } else {
          console.log(result.stdout.toString());
          utils.log('\nFinished initializing git repo', 'green');
        }

        return Promises.resolve();
      });
    }

    return Promises.resolve();
  },

  overwriteFiles: function overwriteFiles(info) {
    var num = info.files && info.files.length;

    var msg = [
      num,
      utils.singularPlural(num, ['file', 'files']),
      'from',
      options.boilerplateName,
      'are already in this project.',
      '\nWould you like to choose which ones to OVERWRITE?'
    ].join(' ');

    return inquirer.prompt([
      {
        message: msg,
        name: 'overwriteFiles',
        type: 'confirm',
        when: !!num
      }
    ])
    .then(function(answers) {
      return inquirer.prompt([
        {
          message: 'Choose the files to overwrite:',
          name: 'overwrite',
          type: 'checkbox',
          choices: info.files,
          when: !!num && answers.overwriteFiles
        }
      ]);
    })
    .then(function(answers) {
      if (answers.overwrite && answers.overwrite.length) {
        return setupTasks.copyFiles(info.files, answers.overwrite, true);
      }

      return Promises.resolve('no');
    });
  },

  copyFiles: function(info) {
    var num = info.extra;
    var msg = [
      num,
      utils.singularPlural(num, ['file', 'files']),
      'from',
      options.boilerplateName,
      'are NOT in this project.',
      '\nWould you like to choose which ones to copy?'
    ].join(' ');

    return inquirer.prompt([
      {
        message: msg,
        name: 'confirmCopyFiles',
        type: 'confirm',
        when: !!num
      }
    ])
    .then(function(answers) {
      return inquirer.prompt([
        {
          message: 'Choose the files to copy:',
          name: 'copyFiles',
          type: 'checkbox',
          choices: info.files,
          when: !!num && answers.confirmCopyFiles
        }
      ]);
    })
    .then(function(answers) {
      if (answers.copyFiles) {
        if (!answers.copyFiles.length) {
          utils.log('Okay. No files to copy. Moving right along…', 'yellow');
        }

        return setupTasks.copyFiles(info.files, answers.copyFiles, false);
      }

      return Promises.resolve('no');
    });
  },
  copyFilesInitial: function copyFilesInitial(answers) {
    if (!answers.copyFilesInitial) {
      return Promises.resolve('no');
    }

    return setupTasks.copyFilesInitial(options);
  },
  updateDependencies: function(answers) {
    var deps = config.devDependencies;
    var installed = pkg.devDependencies || {};
    var modules = utils.getRequiredDependencies(deps, installed);
    var clean = [];

    if (!answers.modules) {
      return Promises.resolve();
    }

    Object.keys(answers)
    .forEach(function(key) {
      var group;
      var addedDeps = [];

      if (key.indexOf('dep-') !== -1) {
        addedDeps = answers[key];
        modules = modules.concat(addedDeps);
      } else if (key.indexOf('depConfirm-') !== -1) {
        group = key.split('-').pop();

        if (answers[key]) {
          addedDeps = utils.getConfirmedDependencies(deps, group);
          modules = modules.concat(addedDeps);
        } else {
          clean = clean.concat(group);
        }
      }
    });

    options = utils.extend(options, {
      modules: modules,
      clean: clean,
      pkg: pkg,
    });

    return Promises.resolve(options.modules);
  },
  cleanup: function(answers) {
    // Bail out if we don't want to clean anything…
    if (!answers.clean) {
      console.log('Okay. NOT cleaning up those files.');

      return Promises.resolve();
    }

    // answers.clean should be Boolean, except for when running tests.
    // For tests, it's an array of "groups" to clean
    if (Array.isArray(answers.clean)) {
      options.clean = answers.clean
    }

    // utils.cleanLines and utils.cleanPackage return options.clean so the next .then() can use it.
    return utils.cleanLines(options.clean)
    .then(utils.cleanPackage)
    .then(utils.cleanFiles);
  },
  updateDotEnv: function(answers) {
    var data = utils.jsonToEnv(answers);

    return fs.writeFileAsync(path.join(cwd, '.env'), data)
    .then(function() {
      utils.log('Successfully updated your .env file:\n', 'green');
      console.log(data);

      return Promises.resolve(answers);
    })
    .catch(utils.logError);
  }
};

var steps = {
  getPackage: function getPackage() {
    return new Promises(function(resolve) {
      try {
        pkg = require(path.join(cwd, 'package.json'));
        resolve(true);
      } catch (err) {
        resolve(false);
      }
    });
  },
  createPackage: function createPackage() {
    var pkgPrompts = questions.package('create');

    return inquirer.prompt(pkgPrompts)
    .then(responses.createPackage);
  },
  checkBoilerplate: function checkBoilerplate(hasPackage) {

    if (!hasPackage) {
      return steps.createPackage()
      .then(steps.installBoilerplate);
    }

    return fs.statAsync(boilerplateDir);
  },

  installBoilerplate: function installBoilerplate() {
    utils.log('Installing ' + boilerplateName + '…', 'yellow');
    pkg.repo = pkg.repository && pkg.repository.url || '';

    return new Promises(function(resolve, reject) {
      exec('npm install -D ' + boilerplateName, function(err) {

        if (err) {
          utils.log(err, 'red');
          reject(err);
        } else {
          options.boilerplateInstalled = true;
          utils.log('Finished installing ' + boilerplateName, 'green');

          resolve({
            isDirectory: function() {
              return true;
            }
          });
        }
      });
    });
  },
  gitInit: function gitInit() {
    var gitStat;
    var gitInitPrompt = questions.gitInit();

    try {
      gitStat = fs.statSync(path.join(cwd, '.git'));
    } catch (err) {
      return inquirer.prompt(gitInitPrompt)
      .then(responses.gitInit);
    }

    return Promises.resolve();
  },

  setRemoteRepo: function setRemoteRepo() {
    return utils.getRepo(pkg)
    .then(function(repos) {
      // If repo is only in .git/config
      if (repos.config && !repos.pkg) {
        return utils.setRepoPkg(pkg, repos);
      }

      // If only in package.json
      if (repos.pkg && !repos.config) {
        return utils.setRepoGitConfig(pkg, repos);
      }

      // If they are DIFFERENT, let user choose which one to use
      if (repos.config !== repos.pkg) {
        return inquirer.prompt(questions.chooseRepo(repos))
        .then(function(answers) {
          if (utils[answers.chooseRepo]) {
            return utils[answers.chooseRepo](pkg, repos);
          }
        });
      }

      // By this time, they are either both there or both not there, so either way, just resolve w/o doing anything
      return Promises.resolve();
    });
  },

  overwriteFiles: function overwriteFiles() {
    return setupTasks.getOverwritable(options)
    .then(responses.overwriteFiles);
  },

  copyFiles: function copyFiles() {
    if (options.boilerplateInstalled) {
      return inquirer.prompt([
        {
          message: 'Would you like to copy files from ' + options.boilerplateName + '?',
          type: 'confirm',
          name: 'copyFilesInitial',
        }
      ])
      .then(responses.copyFilesInitial);
    }

    return setupTasks.getCopyable(options)
    .then(responses.copyFiles);
  },

  updateDependencies: function updateDependencies() {

    // Build list of DEVDEPENDENCIES prompts, in groups to make things more manageable
    pkg.devDependencies = require(path.join(cwd, 'package.json')).devDependencies || {};

    var deps = config.devDependencies;
    var dependencyPrompts = questions.dependencies(deps, pkg.devDependencies, options);
    options.clean = [];

    return inquirer.prompt(dependencyPrompts)
    .then(responses.updateDependencies);
  },

  installModules: function installModules() {
    if (options.modules) {
      return setupTasks.modules(options);
    }
  },

  cleanup: function cleanupFiles() {
    var msg = '\nYou chose not to install packages for ' + chalk.yellow(options.clean.join(', '));
    msg += '\nDo you want to clean up config/gulp files associated with those packages?';

    var cleanFilesPrompt = questions.cleanFiles(msg);

    if (!options.clean || !options.clean.length) {
      return Promises.resolve();
    }

    return inquirer.prompt(cleanFilesPrompt)
    .then(responses.cleanup);
  },

  askDotEnv: function askDotEnv() {
    var askPrompt = questions.askDotEnv();

    return inquirer.prompt(askPrompt)
    .then(function(answers) {
      return Promises.resolve(answers.dotenv);
    });
  },

  updateDotEnv: function updateDotEnv(update) {

    if (!update) {
      return utils.readDotEnvFiles('.env')
      .then(function(json) {
        return Promises.resolve(json);
      });
    }

    return Promises.map(['.env-example', '.env'], function(item) {
      return utils.readDotEnvFiles(item);
    })
    .then(function(results) {
      var json = {};
      var dotEnvPrompts;
      results = results.filter(function(result) {
        return !!result;
      });

      for (var i = 0, len = results.length; i < len; i++) {
        for (var el in results[i]) {
          json[el] = results[i][el];
        }
      }

      dotEnvPrompts = questions.dotEnv(json, pkg);

      if (!dotEnvPrompts.length) {
        utils.log('Can\'t find any .env properties to update.', 'red');

        return Promises.resolve();
      }

      return inquirer.prompt(dotEnvPrompts)
      .then(responses.updateDotEnv);
    });
  },
  suggestions: function suggestions(envData) {

    var msgs = [];

    if (!pkg.repository || !pkg.repository.url) {
      msgs.push('* Add repository.url to package.json before attempting to deploy.');
    }

    return utils.getRepo('gitconfig')
    .then(function(repo) {
      var noenv;

      if (!repo) {
        msgs.push('* Add a remote repo to your git config:');
        msgs.push('\t$ git remote add origin git@DOMAIN:USERNAME/REPO.git');
      }

      if (envData && envData.HTTP_HOST) {
        msgs.push('* Make sure you have an entry in MAMP (or /etc/hosts) for ' + envData.HTTP_HOST);
      } else {
        noenv = '* Then, make sure you have an entry in MAMP (or /etc/hosts) ';
        noenv += 'that matches the HTTP_HOST in your .env file';
      }

      if (!envData) {
        msgs.push(`* Create a .env file in your project root if not already there.
          Copy .env-example for a start.`);
      }

      if (noenv) {
        msgs.push(noenv);
      }

      msgs.push('\n');
      utils.log('\nSUGGESTIONS:', 'cyan');
      msgs.forEach(function(msg) {
        utils.log(msg, 'yellow');
      });

      return Promises.resolve(envData);
    });
  },
  finish: function finish() {
    utils.log('\n⚡  Finished your project setup. Have a party! ✨', 'green');
  },
};

var runSetup = function runSetup() {

  if (utils.boilerplateWarning(cwd)) {
    return;
  }

  steps.getPackage()
  .then(steps.checkBoilerplate)
  .catch(steps.installBoilerplate)
  .then(function(stats) {

    if (!stats || !stats.isDirectory()) {
      throw new Error('You must have the ' + boilerplateName + ' module installed');
    }
  })
  .then(steps.gitInit)
  .then(steps.setRemoteRepo)
  .then(steps.overwriteFiles)
  .then(steps.copyFiles)
  .then(steps.updateDependencies)
  .then(steps.installModules)
  .then(steps.cleanup)
  .then(steps.askDotEnv)
  .then(steps.updateDotEnv)
  .then(steps.suggestions)
  .then(steps.finish);
};

module.exports = {
  init: runSetup,
  steps: steps,
  responses: responses
};
