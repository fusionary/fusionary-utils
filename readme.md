# Fusionary Utilities

Consists of two utilities: `bump` and `setup`, both of which can be run from the command line.

## Installation

`npm install -g fusionary-utils`


## bump

The `bump` command updates the version number in package.json and any other json file you specify.

### Usage

`bump releaseType [files]... [options]`

* **`releaseType`**: The semver-compatible release type. One of: `major`, `minor`, `patch`, `premajor`, `preminor`, `prepatch`. *OR* a valid version.
* **`files`**: List of .json files that, along with the source file (`sourceFile`), will be bumped.

* **Options**
    * `-s`, `--sourceFile`:   File containing the "canonical" version. All files will be bumped relative to it. Default: package.json
    * `-m`, `--message`:      Message to be used for the commit and tag when `-r` or `-p` is set. Default: Release %s
    * `-r`, `--release`:      If set, runs `git add` and `git commit` for the bumped files and pushes a tagged release.
    * `-p`, `--publish`:      If set, automatically runs with the `--release` flag and then publishes the release to npm.
    * `-d`, `--debug`:        If set, bump will run in debug mode, outputting a json file instead of doing something
    * `-h`, `--help`:         Shows help information on the command line

#### .bumprc

In addition to command-line options, you may use a `.bumprc` file in your project's root. The format of this file may be valid JSON or INI.

Example .bumprc using JSON:

```
{
  "message": "Release v%s.",
  "publish": true
}
```

Example .bumprc using INI:

```
message="Release v%s."
publish=true
```

## setup

The `setup` command sets up a new project or updates an existing one.
