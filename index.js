
var msg = [
  'fusionary-utils is a suite of tools for use by Fusionary employees.',
  'If installed globally (npm install -g fusionary-utils), you can use the following commands:',
  '\tbump',
  '\tsetup'
];

console.log(msg.join('\n'));
